﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pAttack : MonoBehaviour
{
    public bool attacking = false;
    private float waitTime = 0.8f;
    public float timer = 0.0f;
    public float initTime = 0.0f;
    private Rigidbody2D rb;
    Animator animator;

    [SerializeField] private Transform attackCheck;
    [SerializeField] private float lengthBox = 0.03f;
    [SerializeField] private LayerMask hitLayer;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        timer += Time.deltaTime;
        if (Input.GetButtonDown("Fire1"))
        {
            attacking = true;
            initTime = timer;
            Collider2D[] damage = Physics2D.OverlapCircleAll(attackCheck.position, lengthBox, hitLayer);
            foreach (Collider2D enemy in damage)
            {
                //Debug.Log("BRFROOOO");
                enemy.GetComponent<eDead>().Die();
            }
        }
        if (/*Input.GetButtonUp("Fire1") & */(timer-initTime)>waitTime)
        {
            attacking = false;
        }
    }

    private void FixedUpdate()
    {
        animator.SetBool("attacking", attacking);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(attackCheck.position, lengthBox);
    }
}
