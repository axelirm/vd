﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eDead : MonoBehaviour
{
    private Rigidbody2D rb;
    Animator animator;
    private float waitTimeE = 2.0f;
    public float timerE = 0.0f;
    public float initTimeE = 0.0f;
    public GameObject enemy1;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Die()
    {
        animator.SetBool("dead", true);
        StartCoroutine(waiter());

        /*initTimeE = Time.deltaTime;
        while((initTimeE-timerE) < waitTimeE)
        {
            //Destroy(this.gameObject);
            timerE = Time.deltaTime;
        }*/
    }

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }
}
